#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
const int MAX = 100;
void menu(void);
void addStudent(char id[][MAX], char name[][MAX],int age[],float mark[MAX], int* size);
void printStudent(char id[][MAX], char name[][MAX],int age[],float mark[MAX], int size);
int searchIDStudent(char id[][MAX],char name[][MAX],int age[],float mark[MAX], int size);
void showInfo(char id[][MAX],char name[][MAX],int age[],float mark[MAX], int pos);
void updateStudent(char id[][MAX],char name[][MAX],int age[],float mark[MAX], int pos);
void deleteStudent(char id[][MAX],char name[][MAX],int age[],float mark[MAX], int pos, int *size);
int main()
{
    //tao cac thuoc tinh cua sinh vien
    char id[MAX][10];
    char name[MAX][40];
    int age[MAX];
    float mark[MAX];
    int size = 0;
    int choose;
    char buffer;

  do{
    menu();
    do{
      printf("\nCHOOSE THE PART U WANNA SEE: ");
      scanf("%d", &choose);
      scanf("%c", &buffer);
      fflush(stdin);
      if(buffer != 10){
        printf("\nERROR");
      }
    }while(buffer != 10);
    switch(choose){
        case 1:{
            printf("\n1 Add STUDENT ");
            addStudent(id,name,age,mark,&size);
            break;
        }
        case 2:{
            printf("\n2 PRINT LISTOFSTUDENT");
            printStudent(id,name,age,mark,size);
            break;
        }
        case 3:{
            printf("\n3 SEARCH STUDENT FSTUDENT ");
            int pos = searchIDStudent(id,name,age,mark,size);
            if(pos == -1){
                printf("\nNo Student to search");
            }else{
                showInfo(id,name,age,mark,pos);
            }
            break;
        }
        case 4:{
            printf("\n4 SEARCH AND UPDATE STUDENT");
             int pos = searchIDStudent(id,name,age,mark,size);
            if(pos == -1){
                printf("\nNo Student to search");
            }else{
                updateStudent(id,name,age,mark,pos);
                printf("\nTHONG TIN SINH VIEN SAU UPDATE");
                showInfo(id,name,age,mark,pos);
                }
            break;
        }
        case 5:{
            printf("\n5 SEARCH AND DELETE STUDENT");
            int pos = searchIDStudent(id,name,age,mark,size);
            if(pos == -1){
                printf("\nNo Student to search");
            }else{
                deleteStudent(id,name,age,mark,pos,&size);
                printf("\n DA XOA THANH CONG");
            }
            break;
        }
        case 6:{
            printf("\n6 :===============HAVE A GOOD DAY SIR===============");
            break;
        }
        default:{
            printf("\n DO YOU KNOW INPUT ");
        }
    }
  }while( choose != 6);
  return 0;
}
void menu(void){
        printf("\n=================STUDENT MANANGERMENT===============");
        printf("\n1 : ADD STUDENT ");
        printf("\n2 : PRINT LISTOFSTUDENT ");
        printf("\n3 : SEARCH STUDENT FSTUDENT ");
        printf("\n4 : SEARCH AND UPDATE STUDENT");
        printf("\n5 : SEARCH AND DELETE STUDENT");
        printf("\n6 :===============HAVE A GOOD DAY SIR===============");
}

void addStudent(char id[][MAX], char name[][MAX],int age[],float mark[MAX], int* size){
    printf("\nNhap vao ID cua sinh vien: ");
    gets(id[*size]);
    fflush(stdin);

    printf("\nNhap vao ten cua sinh vien: ");
    gets(name[*size]);
    fflush(stdin);

    printf("\nNhap vao yob cua sinh vien: ");
    scanf("%d", &age[*size]);
    fflush(stdin);

    printf("\nNhap vao mark cua sinh vien: ");
    scanf("%f", &mark[*size]);
    fflush(stdin);

    *size = *size + 1;
}

void printStudent(char id[][MAX], char name[][MAX],int age[],float mark[MAX], int size){
    if(size <= 0){
        printf("\ndont have anybody in list");
    }else{
        for(int i = 0; i<= size - 1;i++){
            printf("\n|%-10s|%-30s|%4d|%6.2f|", id[i],name[i],age[i],mark[i]);
        }
    }
}
void showInfo(char id[][MAX],char name[][MAX],int age[],float mark[MAX], int pos){
            printf("\n|%-10s|%-30s|%4d|%6.2f|", id[pos],name[pos],age[pos],mark[pos]);
}


int searchIDStudent(char id[][MAX],char name[][MAX],int age[],float mark[MAX], int size){
    char key[MAX];
    printf("\nNHAP ID SINH VIEN : ");
    gets(key);
    fflush(stdin);
    for(int i = 0; i<= size - 1; i++){
        if(strcmp(key, id[i]) == 0){
            return i;
        }
    }
    return -1;
}
void updateStudent(char id[][MAX],char name[][MAX],int age[],float mark[MAX], int pos){
    printf("\nNhap vao ID cua sinh vien: ");
    gets(id[pos]);
    fflush(stdin);

    printf("\nNhap vao ten cua sinh vien: ");
    gets(name[pos]);
    fflush(stdin);

    printf("\nNhap vao yob cua sinh vien: ");
    scanf("%d", &age[pos]);
    fflush(stdin);

    printf("\nNhap vao mark cua sinh vien: ");
    scanf("%f", &mark[pos]);
    fflush(stdin);
}
void deleteStudent(char id[][MAX],char name[][MAX],int age[],float mark[MAX], int pos, int *size){
    for(int i = pos; i<= *size - 1; i++){
        strcpy(id[i], id[i + 1]);

         strcpy(name[i], name[i + 1]);

         age[i] = age[i + 1];

         mark[i] = mark[i + 1];

    }
    *size = *size - 1;
}


    //C             R                                       U                       D
    //create/add    read/printf/in ra danh sách/search      update/edit/repair      Delete/drop

    //làm menu
    //1.Add sinh vien
    //2.in ra danh sách sinh vien
    //3.tìm kiếm sinh viên dựa trên ID
    //4.tìm kiếm sinh viên dựa trên ID và update
    //5.tìm kiếm sinh viên dựa trên ID và delete

    //hay nhap vao ma so sinh vien ban muon tim kiem
    //char key[MAX];
    //em phai thu vien strcmp(key,id[i])
